import flask

app = flask.Flask(__name__)

@app.route("/")
def ola():
    mensagen = "Programação de Redes com Python!"
    return flask.render_template("index.html", mensagen=mensagen)

@app.route("/uper")
def maiusc():
    json = flask.request.get_json()

    mensagen = str(json['message'].upper())

    return mensagen

app.run(host="0.0.0.0/0", port=80)