FROM python:3

COPY app app

WORKDIR /app

RUN pip install flask

CMD [ "python", "app.py" ]